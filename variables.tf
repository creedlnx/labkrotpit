variable "aws_region" {
  type = string
  default = "us-east-1"
}

variable "availability_zones" {
  type = string
  default = "us-east-1a"
}

variable "environment" {
  type = string
  default = "DEVELOP"
}

variable "tag_name" {
  type = string
  default = "Pitagoras"
}

variable "vpc_id" {
    type = string
    default = ""
}

variable "subnet_id" {
    type = string
    default = ""
}

variable "instance_type" {
    type = string
    default = "t2.micro"
}

variable "ebs_size" {
    default = 8
}

variable "ebs_type" {
    type = string
    default = "gp2"
}

variable "ebs_device_name" {
    type = string
    default = "/dev/xvda"
}

variable "instance_ami" {
    type = string
    default = "ami-0c958e37cf2d7b161"
}
