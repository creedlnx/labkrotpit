provider "aws" {
  version                 = "~> 2.0"
  region                  = "${var.aws_region}"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "default"
}

resource "aws_instance" "create_ec2" {
  ami = "${var.instance_ami}"
  instance_type = "${var.instance_type}"
  security_groups = ["${aws_security_group.set_sg.id}"]
  subnet_id = "${var.subnet_id}"
  user_data = "${file("userdata.sh")}"

  ebs_block_device {
    device_name = "${var.ebs_device_name}"
    volume_size = "${var.ebs_size}"
    volume_type = "${var.ebs_type}"
    delete_on_termination = true
  }
  tags = {
    Name = "${var.tag_name}"
    Environment = "${var.environment}"
  }
}
