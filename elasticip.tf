resource "aws_eip" "default" {
  instance = "${aws_instance.create_ec2.id}"
  vpc      = true
}