#!/bin/bash

cat << EOF >> /home/ec2-user/.ssh/authorized_keys
ssh-rsa $SSH_PUBLIC_KEY
EOF
